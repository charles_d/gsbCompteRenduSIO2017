<?php
/**
 * Created by PhpStorm.
 * User: TI-tygangsta
 * Date: 18/06/2018
 * Time: 15:27
 */

namespace Tests\MainBundle\Repository;


use MainBundle\Entity\Motif;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MotifRepositoryTest extends KernelTestCase
{

    private $em;

    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindAll(){
        $motifs = $this->em
            ->getRepository(Motif::class)
            ->findAll();

        $this->assertNotEmpty($motifs);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}